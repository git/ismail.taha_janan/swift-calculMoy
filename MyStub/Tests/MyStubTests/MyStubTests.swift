import XCTest
@testable import MyStub

final class MyStubTests: XCTestCase {
    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct
        // results.
        XCTAssertEqual(MyStub().text, "Hello, World!")
    }
}
