import MyModel
import Foundation


public struct MyStub {
    public static let uesData:[UE] = [
        UE(
            id: UUID(),
            ueName: "UE - 1",
            coef: 9,
            modules: [
                Module(id: UUID(), moduleName: "Module - 1", coef: 2, note: 4),
                Module(id: UUID(), moduleName: "Module - 2", coef: 3, note: 14),
                Module(id: UUID(), moduleName: "Module - 3", coef: 5, note: 10),
                Module(id: UUID(), moduleName: "Module - 4", coef: 2, note: 9),
            ]
        ),
        UE(
            id: UUID(),
            ueName: "UE - 1",
            coef: 9,
            modules: [
                Module(id: UUID(), moduleName: "Module - 1", coef: 2, note: 4),
                Module(id: UUID(), moduleName: "Module - 2", coef: 3, note: 14),
                Module(id: UUID(), moduleName: "Module - 3", coef: 5, note: 10),
                Module(id: UUID(), moduleName: "Module - 4", coef: 2, note: 9),
            ]
        ),
        UE(
            id: UUID(),
            ueName: "UE - 2",
            coef: 9,
            modules: [
                Module(id: UUID(), moduleName: "Module - 1", coef: 2, note: 4),
                Module(id: UUID(), moduleName: "Module - 2", coef: 3, note: 14),
                Module(id: UUID(), moduleName: "Module - 3", coef: 5, note: 10),
                Module(id: UUID(), moduleName: "Module - 4", coef: 2, note: 9),
            ]
        ),
        UE(
            id: UUID(),
            ueName: "UE - 3",
            coef: 9,
            modules: [
                Module(id: UUID(), moduleName: "Module - 1", coef: 2, note: 4),
                Module(id: UUID(), moduleName: "Module - 2", coef: 3, note: 14),
                Module(id: UUID(), moduleName: "Module - 3", coef: 5, note: 10),
                Module(id: UUID(), moduleName: "Module - 4", coef: 2, note: 9),
            ]
        ),
        UE(
            id: UUID(),
            ueName: "UE - 4",
            coef: 9,
            modules: [
                Module(id: UUID(), moduleName: "Module - 1", coef: 2, note: 4),
                Module(id: UUID(), moduleName: "Module - 2", coef: 3, note: 14),
                Module(id: UUID(), moduleName: "Module - 3", coef: 5, note: 15),
                Module(id: UUID(), moduleName: "Module - 4", coef: 2, note: 9),
            ]
        ),
        UE(
            id: UUID(),
            ueName: "UE - 5",
            coef: 9,
            modules: [
                Module(id: UUID(), moduleName: "Module - 1", coef: 2, note: 4),
                Module(id: UUID(), moduleName: "Module - 2", coef: 3, note: 14),
                Module(id: UUID(), moduleName: "Module - 3", coef: 5, note: 10),
                Module(id: UUID(), moduleName: "Module - 4", coef: 2, note: 9),
            ]
        ),
        
    ]

    public init() {
    }
}
