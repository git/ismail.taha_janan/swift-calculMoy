//
//  Module.swift
//  CalculMoy
//
//  Created by etudiant on 11/06/2023.
//

import Foundation

public struct Module{
    public let id: UUID
    public var moduleName: String
    public var coef: Int
    public var note: Double?
    
    public init(id: UUID, moduleName: String, coef: Int, note: Double? = nil) {
        self.id = id
        self.moduleName = moduleName
        self.coef = coef
        self.note = note
    }
    
}
