//
//  File.swift
//  
//
//  Created by etudiant on 14/06/2023.
//

import Foundation
public struct UE {
    public let id: UUID
    public var ueName: String
    public var coef: Int
    public var note: Double?
    public var modules: [Module]
    
    public init(id: UUID, ueName: String, coef: Int, modules: [Module]) {
        self.id = id
        self.ueName = ueName
        self.coef = coef
        self.modules = modules
    }
    
    
}
