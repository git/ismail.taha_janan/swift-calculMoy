//
//  CalculMoyApp.swift
//  CalculMoy
//
//  Created by etudiant on 14/06/2023.
//

import SwiftUI

@main
struct CalculMoyApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
