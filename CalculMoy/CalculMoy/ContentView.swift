//
//  ContentView.swift
//  CalculMoy
//
//  Created by etudiant on 25/05/2023.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        
        
        NavigationStack{
            
            ScrollView(){
                VStack(alignment: .leading){
                    Text("UEs")
                        .font(.largeTitle)
                        .bold()
                        .padding(.leading, 15.0)
                        .frame(alignment: .leading)
                    
                    Text("Détail des UEs")
                        .padding(.leading, 15.0)
                        .frame(alignment: .leading)
                    
                    Group{
                        Block(title: "UE - Génie logiciel", coeff: 6,note: 14)
                        Block(title: "UE - Génie logiciel", coeff: 6,note: 9)
                        Block(title: "UE - Génie logiciel", coeff: 9,note: 18)
                        Block(title: "UE - Génie logiciel", coeff: 9,note: 18)
                        Block(title: "UE - Génie logiciel", coeff: 9,note: 18)
                    }
                    Group{
                        Block(title: "UE - Génie logiciel", coeff: 9,note: 18)
                        Block(title: "UE - Génie logiciel", coeff: 9,note: 18)
                        Block(title: "UE - Génie logiciel", coeff: 9,note: 18)
                        Block(title: "UE - Génie logiciel", coeff: 9,note: 18)
                        Block(title: "UE - Génie logiciel", coeff: 9,note: 18)
                    }
                    
                    
                    
                    
                }
                

                
                
            }.multilineTextAlignment(.leading)
                .navigationTitle("Calculette")
                
            
            
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
