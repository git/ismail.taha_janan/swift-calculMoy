//
//  UsList.swift
//  CalculMoy
//
//  Created by etudiant on 02/06/2023.
//

import SwiftUI

struct UsList: View {
    var body: some View {
        NavigationStack{
            
            ScrollView(){
                VStack(alignment: .leading){
                    Text("UE- ")
                        .font(.largeTitle)
                        .bold()
                        .padding(.leading, 15.0)
                        .frame(alignment: .leading)
                    
                    Text("Détail des UEs")
                        .padding(.leading, 15.0)
                        .frame(alignment: .leading)
                    
                    Group{
                        Block(title: "UE - Génie logiciel", coeff: 6,note: 14)
                        Block(title: "UE - Génie logiciel", coeff: 6,note: 9)
                        Block(title: "UE - Génie logiciel", coeff: 9,note: 18)
                        Block(title: "UE - Génie logiciel", coeff: 9,note: 18)
                        Block(title: "UE - Génie logiciel", coeff: 9,note: 18)
                    }
                    Group{
                        Block(title: "UE - Génie logiciel", coeff: 9,note: 18)
                        Block(title: "UE - Génie logiciel", coeff: 9,note: 18)
                        Block(title: "UE - Génie logiciel", coeff: 9,note: 18)
                        Block(title: "UE - Génie logiciel", coeff: 9,note: 18)
                        Block(title: "UE - Génie logiciel", coeff: 9,note: 18)
                    }
                    
                    
                    
                    
                }
                

                
                
            }.multilineTextAlignment(.leading)
                .navigationTitle("Details")
                
            
            
        }
    }
}

struct UsList_Previews: PreviewProvider {
    static var previews: some View {
        UsList()
    }
}
