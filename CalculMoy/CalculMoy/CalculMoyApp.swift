//
//  CalculMoyApp.swift
//  CalculMoy
//
//  Created by etudiant on 25/05/2023.
//

import SwiftUI
import MyModel
import MyStub

@main
struct CalculMoyApp: App {
    
    
    var managerVM:ManagerVM=ManagerVM(manager: Manager(ues: MyStub.uesData) )
    
    var body: some Scene {
        WindowGroup {
            UesList(managerVM: managerVM)
        }
    }
}
