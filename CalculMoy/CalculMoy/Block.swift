//
//  Block.swift
//  CalculMoy
//
//  Created by etudiant on 26/05/2023.
//

import SwiftUI

struct Block: View {
    public var title : String
    public var coeff : Int
    public var note : Float
    
    
    var body: some View {
        NavigationStack{
            HStack{
                VStack{
                    HStack{
                        Text(title)
                        Spacer()
                        Text(String(coeff))
                    }
                    .padding(.horizontal)
                    HStack{
                        Rectangle()
                            .frame(width: CGFloat(Int(note)) * 10, height: 30)
                            .cornerRadius(20)
                            .foregroundColor((note < 10 ? .red : .green))
                        Text(String(note))
                        Spacer()
                        
                    }
                    .padding(.horizontal)
                    Divider()
                }
                /*NavigationLink(destination: UEView()){
                    Image(systemName: "square.and.pencil")
                }*/
                Button{
                }label: {
                    Image(systemName: "square.and.pencil")
                }
                
                Divider()
                    .padding(.horizontal)
            }
            
        }
    }
}
struct Block_Previews: PreviewProvider {
    static var previews: some View {
        Block(title: "UE - Génie logiciel", coeff: 6,note: 14)
    }
}
