//
//  UesList.swift
//  CalculMoy
//
//  Created by etudiant on 13/06/2023.
//

import SwiftUI
import MyModel
import MyStub

struct UesList: View {
    
    @ObservedObject var managerVM: ManagerVM
    
    var body: some View {
        NavigationStack{
            ScrollView(){
                
                VStack(alignment: .leading){
                    Text("Blocs")
                        .font(.title)
                        .padding(.bottom)
                    
                    Text("Vous devez avoir la moyenne à chacun de ces blocs pour avoir votre diplôme.")
                        .font(.subheadline)
                        .padding(.bottom)
                    
                    
                    if let moyGen = managerVM.getMoyGeneral(){
                        HStack {
                            Text("Total")
                            Spacer()
                            Text(String(format: "%.2f",moyGen))
                            Image(systemName: moyGen >= 10 ? "graduationcap.fill" : "exclamationmark.bubble.fill")
                        }
                    }else{
                        Text("noData")
                    }
                    
                    
                    
                }.padding()
                
                
                VStack(alignment: .leading){
                    
                    
                    Text("UEs")
                        .font(.title)
                        .padding(.bottom)
                    
//                    for ue in managerVM.ueList {
//                        Text(ue.ueName)
//                    }
                    
                    ForEach (0..<managerVM.ueList.count){ue in
                        //Text(managerVM.ueList[ue].ueName)
                        NavigationLink(destination:
                                        UeDetails(managerVM: managerVM,ue: managerVM.ueList[ue])
                        ){
                            UEView(ueVm: managerVM.ueList[ue], managerVm: managerVM)
                        }
                    }
                    
                }.padding()
                
            }.navigationTitle("Calculette")
        }
    }
}

struct UesList_Previews: PreviewProvider {
    static var managerData:ManagerVM=ManagerVM(
        manager: Manager(ues: MyStub.uesData)
        )
    static var previews: some View {
        UesList(managerVM: managerData)
    }
}
