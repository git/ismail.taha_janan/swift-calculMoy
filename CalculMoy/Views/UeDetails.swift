//
//  UeDetails.swift
//  CalculMoy
//
//  Created by etudiant on 13/06/2023.
//

import SwiftUI
import MyModel
import MyStub

struct UeDetails: View {
    
    @ObservedObject var managerVM: ManagerVM
    @ObservedObject var ue:UeVM
    
    
    var body: some View {
        VStack(alignment: .leading){
            Text(ue.ueName)
                .font(.title)
                .padding(.bottom)
            UEView(ueVm: ue, managerVm: managerVM)
            Divider()
            ScrollView{
                
                ForEach (0..<ue.moduleVMs.count){module in
                    //Text(managerVM.ueList[ue].ueName)
                    
                    ModuleView(moduleVM: ue.moduleVMs[module], ueVm: ue, managerVm: managerVM)
                    
                        
                    
                }
                
            }
            
        }.padding()
        
        
        
    }
}

struct UeDetails_Previews: PreviewProvider {
    static var managerData:ManagerVM=ManagerVM(
        manager: Manager(ues: MyStub.uesData)
        )
    static var previews: some View {
        UeDetails(managerVM: managerData,ue: managerData.ueList[0])
    }
}
