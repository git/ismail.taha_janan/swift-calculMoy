//
//  UEView.swift
//  CalculMoy
//
//  Created by etudiant on 12/06/2023.
//

import SwiftUI
import MyModel
import MyStub

struct UEView: View {
    
    @ObservedObject var ueVm: UeVM
    @ObservedObject var managerVm: ManagerVM
    
    var body: some View {
        VStack{
            
            HStack{
                Text("> " + ueVm.ueName)
                Spacer()
                Text(String(ueVm.coef))
            }
            
            HStack{
                if let note = ueVm.calculMoy(){
                    ProgressView(value: note,total: 20)
                        .accentColor(note < 10 ? .red : .green)
                        .scaleEffect(x: 1, y: 4, anchor: .center)
                     Text(String(format: "%.2f",note))
                }
                
            }
            
            
        }
        .padding()
        .background(Color.gray.opacity(0.2))
        .cornerRadius(12)
        .padding(.horizontal)
    }
}

struct UEView_Previews: PreviewProvider {
    
    static var managerData:ManagerVM=ManagerVM(
        manager: Manager(ues: MyStub.uesData)
        )
    
    static var previews: some View {
        UEView(
            ueVm: UeVM(ue: MyStub.uesData[0]) , managerVm: managerData
        )
    }
}
