//
//  MySlider.swift
//  CalculMoy
//
//  Created by etudiant on 12/06/2023.
//

import SwiftUI

struct UISliderView: UIViewRepresentable {

    @Binding var value: Double
    @Binding var editable: Bool

    

    var minValue = 1.0

    var maxValue = 100.0

    var thumbColor: UIColor = UIColor(Color.black.opacity(0))

    var minTrackColor: UIColor = .red

    var maxTrackColor: UIColor = .lightGray
    
    

    

    class Coordinator: NSObject {

        var value: Binding<Double>

        

        init(value: Binding<Double>) {

            self.value = value

        }

        

        @objc func valueChanged(_ sender: UISlider) {

            self.value.wrappedValue = Double(sender.value)

        }

    }

    

    func makeCoordinator() -> UISliderView.Coordinator {

        Coordinator(value: $value)

    }

    

    func makeUIView(context: Context) -> UISlider {

        let slider = UISlider(frame: .zero)

        slider.thumbTintColor = thumbColor

        slider.minimumTrackTintColor = minTrackColor

        slider.maximumTrackTintColor = maxTrackColor

        slider.minimumValue = Float(minValue)

        slider.maximumValue = Float(maxValue)
        
        
        
        

        
        
        if editable{
            slider.value = Float(value)
        }
        
        
        

        

        slider.addTarget(

            context.coordinator,

            action: #selector(Coordinator.valueChanged(_:)),

            for: .valueChanged

        )

        

        return slider

    }

    

    func updateUIView(_ uiView: UISlider, context: Context) {
        
        

        uiView.value =  Float(value)
        uiView.minimumTrackTintColor = value > 10 ? .green : .red

    }

}


struct UISliderView_Previews: PreviewProvider {
     private var currentValue = 6.0

    static var previews: some View {
//        UISliderView(value:  Binding(
//            get: {currentValue},set: { value in
//                currentValue
//            }
//
//
//        ),
//
//                     minValue: 1.0,
//
//                     maxValue: 10.0,
//
//                     thumbColor: .red,
//
//                     minTrackColor: .purple,
//
//                     maxTrackColor: .green)
        Text("je")
    }
}
