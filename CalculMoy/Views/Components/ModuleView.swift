//
//  ModuleView.swift
//  CalculMoy
//
//  Created by etudiant on 12/06/2023.
//

import SwiftUI
import MyModel
import MyStub


final class Slider: UISlider {
 
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        setup()
    }
 
    private func setup() {
        clear()
    }
 
    private func clear() {
        tintColor = .clear
        maximumTrackTintColor = .clear
        backgroundColor = .clear
        thumbTintColor = .clear
    }
}


struct ModuleView: View {
    
    @ObservedObject var moduleVM:ModuleVM
    @ObservedObject var ueVm: UeVM
    @ObservedObject var managerVm: ManagerVM
    @State private var editable:Bool=false
    
    
    
    
    var body: some View {
        
        HStack{
            
        
            
            VStack{
                
                HStack{
                    Text("> " + moduleVM.moduleName)
                    Spacer()
                    Text(String(moduleVM.coef))
                }
                
                HStack{
                    
                    if let note = moduleVM.note{
                        
                        UISliderView(
                            value: Binding(
                                get: {note}, set: {
                                    newValue in
                                    if editable{
                                        moduleVM.note = newValue
                                        ueVm.updateModule(modulVM: moduleVM)
                                        managerVm.updateNoteModule(ueVM: ueVm, moduleVM: moduleVM)
                                    }
                                    
                                } ),
                            editable: $editable,
                            minValue: 0.0,
                            maxValue: 20.00
                            
                            
                        ).disabled(!editable)
                            .scaleEffect(x: 1, y: 4, anchor: .center)
                        
    
                        
                        
                        
                    }
                    if let note = moduleVM.note{
                        
                        Text(String(format: "%.2f",note))
                    }
                    
                    
                }
                
                
                
                
                
            }.padding(10)
            Button{
                //do somthing
                editable = !editable
            }label: {
                Image(systemName: "square.and.pencil")
            }.foregroundColor(
                editable ? .green : .gray
            )
            
        }
        
    }
}

struct ModuleView_Previews: PreviewProvider {
    
    static var managerData:ManagerVM=ManagerVM(
        manager: Manager(ues: MyStub.uesData)
        )
    static var previews: some View {
        ModuleView(
            moduleVM: ModuleVM(module: MyStub.uesData[0].modules[0]), ueVm: UeVM(ue: MyStub.uesData[0]), managerVm: managerData
        )
    }
}
