//
//  ModuleVM.swift
//  CalculMoy
//
//  Created by etudiant on 11/06/2023.
//

import Foundation
import MyModel



class ModuleVM : ObservableObject{
    let id: UUID
    @Published var moduleName: String
    @Published var coef: Int
    @Published var note: Double?
    
    
    public init(id: UUID, moduleName: String, coef: Int, note: Double? = nil) {
        self.id = id
        self.moduleName = moduleName
        self.coef = coef
        self.note = note
    }
    
    public init(module: Module){
        self.id = module.id
        self.moduleName = module.moduleName
        self.coef = module.coef
        self.note = module.note
    }
    
    public func updateModule(name:String,coef:Int,note:Double?){
        self.moduleName = name
        self.coef = coef
        self.note = note
    }
    
}
