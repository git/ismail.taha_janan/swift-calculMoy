//
//  UeVM.swift
//  CalculMoy
//
//  Created by etudiant on 11/06/2023.
//

import Foundation
import MyModel

class UeVM:ObservableObject{
    
    let id: UUID
    @Published var ueName: String
    @Published var coef: Int
    @Published var note: Double?
    @Published var moduleVMs: [ModuleVM]
    
    func calculMoy()->Double?{
        var coef = 0
        var somme = 0.0
        
        for module in moduleVMs {
            if let note = module.note  {
                coef += module.coef
                somme += note * Double(module.coef)
            }
            
        }
        
        if coef == 0 {
            return nil
        }
        
        return somme / Double(coef)
        
    }
    
    public init(id: UUID, ueName: String, coef: Int, modules: [ModuleVM]) {
        self.id = id
        self.ueName = ueName
        self.coef = coef
        self.moduleVMs = modules
        self.note = calculMoy()
    }
    
    public init (ue: UE){
        id = ue.id
        ueName = ue.ueName
        coef = ue.coef
        //note = ue.note
        moduleVMs=[]
        for module in ue.modules {
            moduleVMs.append(ModuleVM(module: module))
        }
        self.note = calculMoy()
    }
    
    
    
    public func addModuleVM(moduleVM: ModuleVM){
        moduleVMs.append(moduleVM)
    }
    
    public func deleteModuleVM(modduleVM: ModuleVM){
        guard let x = getIndexById(id: modduleVM.id) else {return}
        moduleVMs.remove(at: x)
    }
    
    func getIndexById(id:UUID)->Int?{
        return moduleVMs.firstIndex(where: {$0.id == id})
    }
    
    public func updateModule(modulVM: ModuleVM){
        
        for obj in moduleVMs{
            
            if obj.id == modulVM.id{
                obj.updateModule(name: modulVM.moduleName, coef: modulVM.coef, note: modulVM.note)
                self.note = calculMoy()
            }
            
        }
        
    }
    
}
