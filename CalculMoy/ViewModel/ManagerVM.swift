//
//  ManagerVM.swift
//  CalculMoy
//
//  Created by etudiant on 11/06/2023.
//

import Foundation
import MyStub
import MyModel

class ManagerVM:ObservableObject{
    @Published var ueList: [UeVM]
    @Published var moyGeneral: Double?
    
    init(ueList: [UeVM]) {
        self.ueList = ueList
        self.moyGeneral = getMoyGeneral()
        
    }
    
    init(manager:Manager){
        ueList=[]
        for ue in manager.ues{
            ueList.append(UeVM(ue: ue))
        }
        self.moyGeneral = getMoyGeneral()
    }
    
    init(){
        ueList=[]
        addUEs(ues: MyStub.uesData)
        self.moyGeneral = getMoyGeneral()
    }
    
    func addUEs(ues: [UE]){
        for ue in ues {
            ueList.append(UeVM(ue: ue))
        }
        
        self.moyGeneral = getMoyGeneral()
    }
    
    public func updateNoteModule(ueVM: UeVM, moduleVM:ModuleVM){
        
        for obj in ueList{
            if obj.id == ueVM.id{
                obj.updateModule(modulVM: moduleVM)
            }
        }
        
        self.moyGeneral = getMoyGeneral()
        
    }
    
    
    
    public func getMoyGeneral()->Double?{
        var coef = 0
        var somme = 0.0
        
        
        for ue in ueList {
            if let note = ue.note{
                coef += ue.coef
                somme += note * Double(ue.coef)
            }
        }
        
        if coef == 0 {
            return nil
        }
        
        return somme / Double(coef)
    }
}
