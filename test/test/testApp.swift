//
//  testApp.swift
//  test
//
//  Created by etudiant on 14/06/2023.
//

import SwiftUI

@main
struct testApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
