# Marks Calculation Application

The Marks Calculation Application is a command-line tool designed to help teachers calculate and manage marks for their students. This application provides a convenient way to input and calculate grades, generate reports, and perform various calculations related to student assessments.

## Features

### Input and Calculation of Marks
The application allows teachers to input students' marks for different assignments, tests, and exams. It provides a user-friendly interface where teachers can enter the marks and assign weights to each assessment component. The application then calculates the overall marks based on the weighted averages.

### Grade Calculation
The application includes a grade calculation feature that automatically assigns letter grades based on the calculated marks. Teachers can define grade boundaries and the application will assign the appropriate letter grade to each student based on their performance.
